package mobpro.thefaust.app.ezbilitas;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by MuhammadAditya on 15/02/2017.
 */

public class CustomAdapter extends BaseAdapter {
    Context c;
    ArrayList<KamusData> spacecrafts;
    public CustomAdapter(Context c, ArrayList<KamusData> spacecrafts) {
        this.c = c;
        this.spacecrafts = spacecrafts;
    }
    @Override
    public int getCount() {
        return spacecrafts.size();
    }
    @Override
    public Object getItem(int position) {
        return spacecrafts.get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null)
        {
            convertView= LayoutInflater.from(c).inflate(R.layout.model,parent,false);
        }
        TextView nameTxt= (TextView) convertView.findViewById(R.id.nameTxt);
        ImageView fotoTxt= (ImageView) convertView.findViewById(R.id.fotoTxt);
        final KamusData s= (KamusData) this.getItem(position);
        nameTxt.setText(s.getName().toUpperCase());
//        Glide.with(c).load(s.getFoto()).into(fotoTxt);
        Picasso.with(c)
                .load(s.getFoto())
                .into(fotoTxt); //letakan gambar yang telah diload kedalam img
        //fotoTxt.setText(s.getFoto());
        //ONITECLICK
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(c,s.getName(),Toast.LENGTH_SHORT).show();
            }
        });
        return convertView;
    }
}
