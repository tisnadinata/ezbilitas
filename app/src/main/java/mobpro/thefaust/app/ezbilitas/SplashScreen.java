package mobpro.thefaust.app.ezbilitas;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

/**
 * Created by fadlimaulana on 11/19/2016.
 */

public class SplashScreen  extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent i = new Intent(this,MainActivity.class);
        startActivity(i);
        finish();
    }
}
