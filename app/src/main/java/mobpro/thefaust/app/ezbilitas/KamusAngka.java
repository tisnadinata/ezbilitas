package mobpro.thefaust.app.ezbilitas;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

/**
 * Created by MuhammadAditya on 15/02/2017.
 */

public class KamusAngka extends AppCompatActivity{

    DatabaseReference dref;
    ListView listview;
    ArrayList<KamusData> list = new ArrayList<>();
    CustomAdapter adapter;
    private ProgressDialog mProgressDialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kamus_list);

        mProgressDialog = new ProgressDialog(this);

        mProgressDialog.setMessage("Sedang menyiapkan data...");
        mProgressDialog.show();

        listview = (ListView) findViewById(R.id.kamus_listview);
        adapter = new CustomAdapter(this,list);
        listview.setAdapter(adapter);

        dref = FirebaseDatabase.getInstance().getReference("kamus_angka");
        dref.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                for(DataSnapshot child:dataSnapshot.getChildren()){
                    KamusData value = child.getValue(KamusData.class);
                    list.add(value);
                    adapter.notifyDataSetChanged();
                }
                mProgressDialog.dismiss();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                list.clear();
                for(DataSnapshot child:dataSnapshot.getChildren()){
                    KamusData value = child.getValue(KamusData.class);
                    list.add(value);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    private void reload_page(){
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }
}
