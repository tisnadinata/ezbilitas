package mobpro.thefaust.app.ezbilitas;

import android.provider.BaseColumns;

/**
 * Created by MuhammadAditya on 12/11/2016.
 */

public class DatabaseTabelKamus implements BaseColumns{
    public static final String NAMA = "nama";
    public static final String GAMBAR = "gambar";
    public static final String TABLE_NAME = "kamus";

    public static final String CREATE_QUERY = "CREATE TABLE "+TABLE_NAME+" " +
            "("+_ID+" INTEGER PRIMARY KEY,"+
            NAMA+" TEXT,"+ GAMBAR+" BLOB)";

    public static final String DROP_QUERY = "DROP TABLE "+TABLE_NAME;

    public static final String SELECT_QUERY = "SELECT * FROM "+TABLE_NAME;


}
