package mobpro.thefaust.app.ezbilitas;

import android.view.View;

/**
 * Created by fadlimaulana on 11/18/2016.
 */

public interface ClickListener {
    void onClick(View view, int position);

    void onLongClick(View view, int position);
}