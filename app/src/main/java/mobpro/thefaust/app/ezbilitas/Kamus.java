package mobpro.thefaust.app.ezbilitas;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

/**
 * Created by MuhammadAditya on 16/02/2017.
 */

public class Kamus extends AppCompatActivity {
    Button alfabet,angka,keluarga,sapa,tanya;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_kamus_daftar);

        alfabet = (Button) findViewById(R.id.button_alfabet);
        angka = (Button) findViewById(R.id.button_angka);
        keluarga = (Button) findViewById(R.id.button_keluarga);
        sapa = (Button) findViewById(R.id.button_sapa);
        tanya = (Button) findViewById(R.id.button_tanya);

        alfabet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Kamus.this, KamusAlfabet.class));
            }
        });
        angka.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Kamus.this, KamusAngka.class));
            }
        });
        keluarga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Kamus.this, KamusKeluarga.class));
            }
        });
       sapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Kamus.this, KamusSapa.class));
            }
        });
        tanya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Kamus.this, KamusTanya.class));
            }
        });
    }
}
