package mobpro.thefaust.app.ezbilitas;

/**
 * Created by MuhammadAditya on 12/11/2016.
 */

public class DatabaseKamus {

    int id;
    private String nama,gambar;

    public DatabaseKamus(int id, String nama, String gambar){
        this.id = id;
        this.nama = nama;
        this.gambar = gambar;
    }

    public DatabaseKamus(String nama, String gambar){
        this.nama = nama;
        this.gambar = gambar;
    }

    public int getId() {
        return id;
    }

    public String getNama() {
        return nama;
    }

    public String getGambar() {
        return gambar;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }
}
