package mobpro.thefaust.app.ezbilitas;

/**
 * Created by MuhammadAditya on 18/11/2016.
 */

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

/**
 * Provides UI for the view with Cards.
 */
public class TranslateTextFragment extends Fragment implements View.OnClickListener{
    EditText toTranslate;
    Button btnTranslate;
    ListView lv;
    public TranslateTextFragment(){

    }
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.activity_translate_text, container, false);

        lv =  (ListView) rootView.findViewById(R.id.listViewData);
        toTranslate = (EditText) rootView.findViewById(R.id.txtTranslate);
        btnTranslate = (Button) rootView.findViewById(R.id.btnTranslate);
		btnTranslate.setOnClickListener(this);
        // Inflate the layout for this fragment

        return rootView;
    }

    public void onClick(View view) {
		switch (view.getId()) {
        case R.id.btnTranslate:

            getGambar();
//            getGambarr();

            break;
		}
    }

    public void getGambar(){
        DatabaseHelper db = new DatabaseHelper(getActivity());
        Cursor cur;
        SimpleCursorAdapter adapter;

        cur = db.getAlldata(toTranslate.getText().toString().toLowerCase());
        String[] from = new String[] {DatabaseTabelKamus.GAMBAR};
        int[] to = new int[] {R.id.txtViewNama};
        adapter = new SimpleCursorAdapter(getActivity(),R.layout.list_kamus,cur,from,to,0);

        lv.setAdapter(adapter);
    }
    public void getGambarr(){
        DatabaseHelper db = new DatabaseHelper(getActivity());
        Cursor cur;
        SimpleCursorAdapter adapter;
        char huruf;

        for(int i=0;i<toTranslate.length();i++) {
            huruf = toTranslate.getText().toString().toLowerCase().charAt(i);

            cur = db.getAlldata(Character.toString(huruf));
            String[] from = new String[] {DatabaseTabelKamus.GAMBAR};
            int[] to = new int[] {R.id.txtViewNama};
            adapter = new SimpleCursorAdapter(getActivity(),R.layout.list_kamus,cur,from,to,0);

            lv.setAdapter(adapter);
        }
    }

}
