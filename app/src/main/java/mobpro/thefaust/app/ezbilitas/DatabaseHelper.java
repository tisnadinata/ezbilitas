package mobpro.thefaust.app.ezbilitas;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by MuhammadAditya on 12/11/2016.
 */

public class DatabaseHelper extends SQLiteOpenHelper{

    public DatabaseHelper(Context context) {

        super(context, "dbkamus",null,1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DatabaseTabelKamus.CREATE_QUERY);

        db.execSQL("INSERT INTO kamus(nama,gambar) values('1',"+R.drawable.bisindo_1 +")");
        db.execSQL("INSERT INTO kamus(nama,gambar) values('2',"+R.drawable.bisindo_2 +")");
        db.execSQL("INSERT INTO kamus(nama,gambar) values('3',"+R.drawable.bisindo_3 +")");
        db.execSQL("INSERT INTO kamus(nama,gambar) values('4',"+R.drawable.bisindo_4 +")");
        db.execSQL("INSERT INTO kamus(nama,gambar) values('5',"+R.drawable.bisindo_5 +")");
        db.execSQL("INSERT INTO kamus(nama,gambar) values('6',"+R.drawable.bisindo_6 +")");
        db.execSQL("INSERT INTO kamus(nama,gambar) values('7',"+R.drawable.bisindo_7 +")");
        db.execSQL("INSERT INTO kamus(nama,gambar) values('8',"+R.drawable.bisindo_8 +")");
        db.execSQL("INSERT INTO kamus(nama,gambar) values('9',"+R.drawable.bisindo_9 +")");
        db.execSQL("INSERT INTO kamus(nama,gambar) values('10',"+R.drawable.bisindo_10 +")");
        db.execSQL("INSERT INTO kamus(nama,gambar) values('a',"+R.drawable.bisindo_a +")");
        db.execSQL("INSERT INTO kamus(nama,gambar) values('b',"+R.drawable.bisindo_b +")");
        db.execSQL("INSERT INTO kamus(nama,gambar) values('c',"+R.drawable.bisindo_c +")");
        db.execSQL("INSERT INTO kamus(nama,gambar) values('d',"+R.drawable.bisindo_d +")");
        db.execSQL("INSERT INTO kamus(nama,gambar) values('e',"+R.drawable.bisindo_e +")");
        db.execSQL("INSERT INTO kamus(nama,gambar) values('f',"+R.drawable.bisindo_f +")");
        db.execSQL("INSERT INTO kamus(nama,gambar) values('g',"+R.drawable.bisindo_g +")");
        db.execSQL("INSERT INTO kamus(nama,gambar) values('h',"+R.drawable.bisindo_h +")");
        db.execSQL("INSERT INTO kamus(nama,gambar) values('i',"+R.drawable.bisindo_i +")");
        db.execSQL("INSERT INTO kamus(nama,gambar) values('j',"+R.drawable.bisindo_j +")");
        db.execSQL("INSERT INTO kamus(nama,gambar) values('k',"+R.drawable.bisindo_k +")");
        db.execSQL("INSERT INTO kamus(nama,gambar) values('l',"+R.drawable.bisindo_l +")");
        db.execSQL("INSERT INTO kamus(nama,gambar) values('m',"+R.drawable.bisindo_m +")");
        db.execSQL("INSERT INTO kamus(nama,gambar) values('n',"+R.drawable.bisindo_n +")");
        db.execSQL("INSERT INTO kamus(nama,gambar) values('o',"+R.drawable.bisindo_o +")");
        db.execSQL("INSERT INTO kamus(nama,gambar) values('p',"+R.drawable.bisindo_p +")");
        db.execSQL("INSERT INTO kamus(nama,gambar) values('q',"+R.drawable.bisindo_q +")");
        db.execSQL("INSERT INTO kamus(nama,gambar) values('r',"+R.drawable.bisindo_r +")");
        db.execSQL("INSERT INTO kamus(nama,gambar) values('s',"+R.drawable.bisindo_s +")");
        db.execSQL("INSERT INTO kamus(nama,gambar) values('t',"+R.drawable.bisindo_t +")");
        db.execSQL("INSERT INTO kamus(nama,gambar) values('u',"+R.drawable.bisindo_u +")");
        db.execSQL("INSERT INTO kamus(nama,gambar) values('v',"+R.drawable.bisindo_v +")");
        db.execSQL("INSERT INTO kamus(nama,gambar) values('w',"+R.drawable.bisindo_w +")");
        db.execSQL("INSERT INTO kamus(nama,gambar) values('x',"+R.drawable.bisindo_x +")");
        db.execSQL("INSERT INTO kamus(nama,gambar) values('y',"+R.drawable.bisindo_y +")");
        db.execSQL("INSERT INTO kamus(nama,gambar) values('z',"+R.drawable.bisindo_z +")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DatabaseTabelKamus.DROP_QUERY);
        db.execSQL(DatabaseTabelKamus.CREATE_QUERY);
    }

    /* ----------------------------- TAMPIL SMEUA KONTAK ----------------------------- */
    public Cursor getAlldata(String kata){
        String cari = "nama = '0'";
        for(int i=0;i<kata.length();i++) {
            cari = cari+" OR nama = '"+kata.charAt(i)+"'";
        }
        return this.getWritableDatabase().rawQuery("SELECT * FROM kamus WHERE "+cari,null);
    }

    public Cursor getData(String huruf){
        return this.getWritableDatabase().rawQuery("SELECT * FROM kamus WHERE nama = '"+huruf+"'",null);
    }

}
