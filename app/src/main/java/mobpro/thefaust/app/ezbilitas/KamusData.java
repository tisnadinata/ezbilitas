package mobpro.thefaust.app.ezbilitas;

/**
 * Created by MuhammadAditya on 14/02/2017.
 */

public class KamusData {
    public String name;
    public String url_foto;

    // Default constructor required for calls to
    // DataSnapshot.getValue(User.class)
    public KamusData() {
    }

    public KamusData(String name, String url_foto) {
        this.name = name;
        this.url_foto = url_foto;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getFoto() {
        return url_foto;
    }
    public void setFoto(String url_foto) {
        this.url_foto = url_foto;
    }
}
