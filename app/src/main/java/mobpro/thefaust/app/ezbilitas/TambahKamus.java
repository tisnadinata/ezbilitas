package mobpro.thefaust.app.ezbilitas;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

public class TambahKamus extends AppCompatActivity {
    private Toolbar toolbar;
    private Spinner kategori_kamus;
    private Button tambah_kamus,pilih_gambar;
    private EditText nama_kamus;
    private ImageView data_gambar;
    private FirebaseDatabase database;
    private DatabaseReference myRef;
    private static int RESULT_LOAD_IMAGE = 1;
    private StorageReference mStorageRef;
    private ProgressDialog mProgressDialog;
    private Uri selectedImage;
    private int stat_gambar = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_kamus);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        database = FirebaseDatabase.getInstance();
        mStorageRef = FirebaseStorage.getInstance().getReference();

        kategori_kamus = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.kamus, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        kategori_kamus.setAdapter(adapter);

        pilih_gambar = (Button) findViewById(R.id.btn_pilih);
        tambah_kamus = (Button) findViewById(R.id.btn_tambah);
        nama_kamus = (EditText) findViewById(R.id.input_name);
        tambah_kamus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createKamus(nama_kamus.getText().toString(),kategori_kamus.getSelectedItem().toString());
            }
        });

        pilih_gambar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                startActivityForResult(i, RESULT_LOAD_IMAGE);
            }
        });

        mProgressDialog = new ProgressDialog(this);

        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
        }else{
            Toast.makeText(TambahKamus.this, "Anda tidak terkoneksi ke internet,data yang ditambahkan akan disimpan di local storage hingga terkoneksi kembali",Toast.LENGTH_LONG).show();
        }
    }
    private void createKamus(String name, String kategori) {

        if(kategori.equals("Pilih Kamus")){
            Toast.makeText(TambahKamus.this, "Silahkan pilih kategori kamus dulu",Toast.LENGTH_SHORT).show();
        }else if(name.isEmpty()){
            Toast.makeText(TambahKamus.this, "Nama kamus/data tidak boleh kosong",Toast.LENGTH_SHORT).show();
        }else if(stat_gambar == 0){
            Toast.makeText(TambahKamus.this, "Silahkan pilih gambar dulu",Toast.LENGTH_SHORT).show();
        }else{
            try {
                mProgressDialog.setMessage("Sedang menyimpan data...");
                mProgressDialog.show();

                final String new_name = name.replace(' ','_').toLowerCase();
                final String new_kategori = kategori.replace(' ','_').toLowerCase();
                String image_name = new_kategori+"_"+new_name;
                //TO UPLOAD IMAGE
                StorageReference riversRef = mStorageRef.child(image_name);

                riversRef.putFile(selectedImage)
                        .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                // Get a URL to the uploaded content
                                Uri downloadUrl = taskSnapshot.getDownloadUrl();

                                KamusData data = new KamusData(new_name, downloadUrl.toString());
                                myRef = database.getReference("kamus_"+new_kategori);
                                myRef.child(new_kategori).child(new_name).setValue(data);

                                mProgressDialog.dismiss();

                                Toast.makeText(TambahKamus.this, "Data kamus baru berhasil ditambah",Toast.LENGTH_LONG).show();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                // Handle unsuccessful uploads
                                // ...
                                mProgressDialog.dismiss();

                                Toast.makeText(TambahKamus.this, "Maaf, data anda tidak dapat disimpan",Toast.LENGTH_LONG).show();
                            }
                        });

            }catch (Exception e){
                Toast.makeText(TambahKamus.this, "Data "+name+" gagal ditambah",Toast.LENGTH_SHORT).show();
            }
        }

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {

            selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            data_gambar= (ImageView) findViewById(R.id.gambar_kamus);
            data_gambar.setImageBitmap(BitmapFactory.decodeFile(picturePath));

            stat_gambar = 1;
        }
    }
}
