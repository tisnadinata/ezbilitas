package mobpro.thefaust.app.ezbilitas;

/**
 * Created by MuhammadAditya on 18/11/2016.
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * Provides UI for the view with Cards.
 */
public class AboutTimFragment extends Fragment {
    public AboutTimFragment() {

    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_about_tim, container, false);

        ImageView imageView = (ImageView) rootView.findViewById(R.id.imgPadli);
        ImageView imageView2 = (ImageView) rootView.findViewById(R.id.imgAdit);
        ImageView imageView3 = (ImageView) rootView.findViewById(R.id.imgCaca);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(getActivity(),fadli.class);
                startActivity(in);
            }
        });
        imageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(getActivity(),adit.class);
                startActivity(in);
            }
        });
        imageView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(getActivity(),caca.class);
                startActivity(in);
            }
        });
        return rootView;
    }
      /*  RecyclerView recyclerView = (RecyclerView) inflater.inflate(
                R.layout.recycler_view, container, false);
        AboutTimFragment.ContentAdapter adapter = new AboutTimFragment.ContentAdapter(recyclerView.getContext());
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        //recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        return recyclerView;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView picture;
        public ViewHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.item_card, parent, false));
            picture = (ImageView) itemView.findViewById(R.mobpro.card_image);
        }
    }



    public static class ContentAdapter extends RecyclerView.Adapter<AboutTimFragment.ViewHolder> {
        // Set numbers of List in RecyclerView.
        private static int LENGTH = 26;
        private final Drawable[] mPlacePictures;
        public ContentAdapter(Context context) {
            Resources resources = context.getResources();
            TypedArray a = resources.obtainTypedArray(R.array.alfabet_picture);
            mPlacePictures = new Drawable[a.length()];
            this.LENGTH = mPlacePictures.length;
            for (int i = 0; i < mPlacePictures.length; i++) {
                mPlacePictures[i] = a.getDrawable(i);
            }
            a.recycle();
        }

        @Override
        public AboutTimFragment.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new AboutTimFragment.ViewHolder(LayoutInflater.from(parent.getContext()), parent);
        }

        @Override
        public void onBindViewHolder(AboutTimFragment.ViewHolder holder, int position) {
            holder.picture.setImageDrawable(mPlacePictures[position % mPlacePictures.length]);
        }

        @Override
        public int getItemCount() {
            return LENGTH;
        }
    }
    }
*/

}


